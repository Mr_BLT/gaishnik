package com.job.test.demo.service.utilCar;

public interface CarNumberService {

  public String nextNumber();

  public String randomNumber();
}
