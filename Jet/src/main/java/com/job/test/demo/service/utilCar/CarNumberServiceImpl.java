package com.job.test.demo.service.utilCar;

import java.util.HashMap;
import org.springframework.stereotype.Service;

@Service
public class CarNumberServiceImpl implements CarNumberService {


  private final int MAX_NUMBER_INT = 1728000;
  private int number = 0;
  private HashMap<Integer, String> hashMap;

  public CarNumberServiceImpl() {
    hashMap = new HashMap<>();

    String[] str = new String[]{"А", "B", "Е", "К", "М", "О", "Р", "С", "Т", "Н", "У", "Х"};
    for (int i = 0; i < str.length; i++) {
      hashMap.put(i, str[i]);
    }
  }

  private String convertoString(int number) {
    int numberInt = number % 1000;
    int numberSymbol = number / 1000;

    String[] symbolString = new String[]{hashMap.get(0), hashMap.get(0), hashMap.get(0)};
    int j = 2;
    while (numberSymbol > 0) {
      symbolString[j] = hashMap.get(numberSymbol % 12);
      j--;
      numberSymbol = numberSymbol / 12;
    }

    String[] stringInt = new String[]{"", "", ""};
    stringInt[2] += numberInt % 10;
    numberInt /= 10;
    stringInt[1] += numberInt % 10;
    numberInt /= 10;
    stringInt[0] += numberInt;

    String answerString = symbolString[0] + stringInt[0] + stringInt[1] + stringInt[2]
        + symbolString[1] + symbolString[2];
    return answerString;
  }

  @Override
  public String nextNumber() {
    if (number >= MAX_NUMBER_INT) {
      return null;
    }
    String answer = convertoString(number);
    number++;
    return answer;
  }

  @Override
  public String randomNumber() {
    if (number >= MAX_NUMBER_INT) {
      return null;
    }

    int count = getRandomNumber(number, MAX_NUMBER_INT - 1);
    String answer = convertoString(count);

    if (count == number) {
      number++;
    }

    return answer;
  }

  private int getRandomNumber(int min, int max) {
    return (int) ((Math.random() * (max - min)) + min);
  }

}
