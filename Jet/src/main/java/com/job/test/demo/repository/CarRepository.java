package com.job.test.demo.repository;

import com.job.test.demo.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Long> {

  Car findByNumberCar(String numberCar);
}
