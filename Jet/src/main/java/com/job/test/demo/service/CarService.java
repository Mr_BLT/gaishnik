package com.job.test.demo.service;

public interface CarService {
  public String numberCarNext();
  public String numberCarRandom();
}
