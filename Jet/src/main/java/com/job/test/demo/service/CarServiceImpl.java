package com.job.test.demo.service;

import com.job.test.demo.model.Car;
import com.job.test.demo.repository.CarRepository;
import com.job.test.demo.service.utilCar.CarNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarServiceImpl implements CarService {

  @Autowired
  private CarRepository carRepository;

  @Autowired
  private CarNumberService carNumberService;


  @Override
  public String numberCarNext() {
    String numberCar = carNumberService.nextNumber();

    if (numberCar == null) {
      return null;
    }

    while (carRepository.findByNumberCar(numberCar) != null) {
      numberCar = carNumberService.nextNumber();
    }

    Car car = Car.builder()
        .numberCar(numberCar)
        .build();

    carRepository.save(car);

    return numberCar;
  }

  @Override
  public String numberCarRandom() {
    String numberCar = carNumberService.randomNumber();

    if (numberCar == null) {
      return null;
    }

    while (carRepository.findByNumberCar(numberCar) != null) {
      numberCar = carNumberService.randomNumber();
    }

    Car car = Car.builder()
        .numberCar(numberCar)
        .build();

    carRepository.save(car);

    return numberCar;
  }
}
