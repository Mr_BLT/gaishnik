package com.job.test.demo.controller;

import com.job.test.demo.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestControllerNumber {

  @Autowired
  private CarService carService;

  @GetMapping("/number/next")
  public String getNextNumber() {
    String numberCar = carService.numberCarNext();
    if (numberCar == null) {
      numberCar = "Номера закончились";
    }
    return numberCar;
  }

  @GetMapping("/number/random")
  public String getRandomNumber() {
    String numberCar = carService.numberCarRandom();
    if (numberCar == null) {
      numberCar = "Номера закончились";
    }
    return numberCar;
  }
}
